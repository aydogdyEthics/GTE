import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { NaytiPage } from '../pogruzki/nayti/nayti';
import { MoiPogruzkiPage } from '../moiPogruzki/moi-pogruzki/moi-pogruzki';
import { HistoryPage } from '../history/history/history';
import { HelpPage } from '../help/help';
import { ProfilePage } from '../login/profile/profile';
import { LoginPage } from '../login/login/login';
import { PogruzkiPage } from '../pogruzki/pogruzki/pogruzki';
import { PogruzkaPage } from '../pogruzki/pogruzka/pogruzka';

@Component({
  selector: 'page-root-tab',
  templateUrl: 'root-tab.html',
})
export class RootTabPage {

  profile = ProfilePage;
  search = NaytiPage;
  myorders = MoiPogruzkiPage;
  history = HistoryPage;
  help = HelpPage;
  selectedIndx: number = 1;
  login = LoginPage;

  isUserLogedIn: boolean = false;

  constructor(public navCtrl: NavController,
              private events: Events, 
              public navParams: NavParams) {

    let indx = this.navParams.get('index');
    let fromPush = this.navParams.get('fromPush');
    let orderId = this.navParams.get('orderId');
    
    console.log(indx);
    if(indx){
      this.selectedIndx = indx;
      if(fromPush){
        this.navCtrl.push(PogruzkaPage);
        this.events.publish('push:opened', {'orderId': orderId});
      }
    }
    
    let userToken = JSON.parse(localStorage.getItem("gte"));
    if(!userToken && userToken == null){
        this.isUserLogedIn = false;
        this.selectedIndx = 0;
    }else{
      this.isUserLogedIn = true;
    }

    this.events.subscribe('user:login', (e: any) => {
      console.log(e);
      if(e.logedIn){
        this.isUserLogedIn = true;
        this.selectedIndx = 1;
      }else{
        this.isUserLogedIn = false;
        this.selectedIndx = 0;
      }
    })

  }


 
}
