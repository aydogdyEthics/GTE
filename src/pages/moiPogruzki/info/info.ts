import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, App, IonicApp, ViewController, AlertController, Tabs } from 'ionic-angular';
import { PogruzkiService } from '../../../services/pogruzki';
import { Pogruzka } from '../../../models/pogruzki/pogruzka';
import { HistoryService } from '../../../services/history';
import { LocationTracker } from '../../../providers/location-tracker';
import { MainTabs } from '../mainTab';
import { PogruzkaTabs } from '../pogruzkaTab';
import { LaunchNavigator } from '@ionic-native/launch-navigator';


@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  pogruzka: Pogruzka;
  @ViewChild('pogruzkaTabs') tabRef: Tabs;

  constructor(public navCtrl: NavController,
              private pogruzkiS: PogruzkiService,
              private app: App,
              private ionicApp: IonicApp,
              private historyS: HistoryService,
              private viewCtrl: ViewController,
              public locationTracker: LocationTracker,
              private launchNavigator: LaunchNavigator,
              private alertCtrl: AlertController,
              public navParams: NavParams) {

       this.pogruzka = this.pogruzkiS.selectedPogruzka;
  
  }

  
  pogruzkaStateChange(id:number, state: number, text: string){
    if(id == this.pogruzka.id){
      this.pogruzka.status.id = state;
      this.pogruzkiS.selectedPogruzka.status.id = state;
      if(state !==9){
        this.pogruzkiS.myPogruzkiArray[this.pogruzkiS.selectedPogruzkaIndx].status.id = state; 
        this.pogruzkiS.myPogruzkiArray[this.pogruzkiS.selectedPogruzkaIndx].status.text = text; 
      }
      return this.pogruzka, this.pogruzkiS.selectedPogruzka;
    }
  }



  stateChange(id: number, stateName: string){

      this.pogruzkiS.stateChange(id, stateName).then((res: any)=>{
        console.log(res.data);

        if(stateName == "load" || stateName == "unload"){
          this.presentAttachDocAlert();
        }

        if(stateName == "ready-load"){
          this.pogruzkiS.isDriverNearPoint = false;
        }
        if(stateName == "finish"){
          this.app.getRootNav().setRoot(MainTabs);

          this.finishOrder(res.data.status.id, res.data.status.text);
        };
        this.pogruzkaStateChange(id, res.data.status.id, res.data.status.text);
      }, (error: any)=>{
        this.pogruzkiS.presentToast(error.message, "error", "top", false, "", 3000);
        console.log(error);
      });
  }


  finishOrder(id: number, text: string){
    
    let pogHis = {id: this.pogruzka.id,
                  from_note: this.pogruzka.from.note,
                  to_note: this.pogruzka.to.note,
                  date: this.pogruzka.date,
                  summary: this.pogruzka.summary,
                  offer: parseInt(this.pogruzka.offer_price),
                  offer_company: null,
                  offer_price: parseInt(this.pogruzka.offer_price),
                  number: this.pogruzka.number,
                  status:{ id: id, text: text},
                  load_address: this.pogruzka.load_address,
                  load_contact: this.pogruzka.load_contact,
                  load_contact_phone: this.pogruzka.load_contact_phone,
                  unload_address: this.pogruzka.unload_address,
                  unload_contact: this.pogruzka.unload_contact,
                  unload_contact_phone: this.pogruzka.unload_contact_phone
                };
    
    this.historyS.historyArray.unshift(pogHis);

    this.pogruzkiS.isObratnyePogruzkiPage = false;
    this.pogruzkiS.myPogruzkiArray.splice(this.pogruzkiS.selectedPogruzkaIndx, 1);

    if(this.pogruzkiS.myPogruzkiArray.length = 0){
      this.locationTracker.stopTracking();
    }

    this.pogruzkiS.isDriverNearPoint = false;
  }


  navigate(){
    let app: any;
    
    this.launchNavigator.isAppAvailable(this.launchNavigator.APP.YANDEX_NAVIGATOR).then((data)=>{
      app = this.launchNavigator.APP.YANDEX_NAVIGATOR;
    },(error)=>{
      app = this.launchNavigator.userSelect;
    });
    
    this.launchNavigator.navigate([parseInt(this.pogruzkiS.selectedPogruzka.to.lat), parseInt(this.pogruzkiS.selectedPogruzka.to.lng)], {
        start: [parseInt(this.pogruzkiS.selectedPogruzka.from.lat), parseInt(this.pogruzkiS.selectedPogruzka.from.lng)],
        app: app
    }).then(
      success => {
        console.log('Launched navigator')
      },error => {
        let alert = this.alertCtrl.create({
          title: 'Error launching navigator!',
          subTitle: JSON.stringify(error),
          buttons: ['OK']
        });
        alert.present();
        console.log('Error launching navigator', error)
      }
    );
  }


  presentAttachDocAlert(){
    let confirm = this.alertCtrl.create({
      title: 'Сфотографировать груз?',
      buttons: [
        {
          text: 'Да',
          handler: () => {
              this.navCtrl.parent.select(1);
          }
        },
        {
          text: 'Нет',
          role: 'cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

}