import { Component } from '@angular/core';
import { MoiPogruzkiPage } from './moi-pogruzki/moi-pogruzki';
import { PogruzkiPage } from '../pogruzki/pogruzki/pogruzki';
import { PogruzkiService } from '../../services/pogruzki';

@Component({
	selector: 'page-tabs',
    template: `
		<ion-tabs [selectedIndex]="0" #mainTabs  tabsPlacement='bottom' color='tabcolor' tabsLayout="icon-start">
			
			<ion-tab [root]="myPogruzki"
					 tabTitle="Текущий"></ion-tab>
					 
			<ion-tab [root]="pogruzkiPage"
                     tabTitle="Обратный"
                     (ionSelect)="onMyBackPogruzki()"></ion-tab>
		
		</ion-tabs>
	`
})
export class MainTabs {
	
	myPogruzki = MoiPogruzkiPage;
	pogruzkiPage = PogruzkiPage;

    constructor(private pogruzkiS: PogruzkiService){
	}
	
    onMyBackPogruzki(){
        this.pogruzkiS.isObratnyePogruzkiPage = true;
    }


}
