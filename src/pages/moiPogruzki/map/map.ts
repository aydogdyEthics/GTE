import { Component } from '@angular/core';
import { IonicPage, AlertController } from 'ionic-angular';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { PogruzkiService } from '../../../services/pogruzki';


@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage{

  constructor(private launchNavigator: LaunchNavigator,
              private pogruzkiS: PogruzkiService,
              private alertCtrl: AlertController) {

  }


}