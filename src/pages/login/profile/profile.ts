import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';
import { RegFormPage } from '../reg-form/reg-form';
import { ProfileService } from '../../../services/profile';

import { PhotoViewer } from '@ionic-native/photo-viewer';
import { PogruzkiService } from '../../../services/pogruzki';
import { AuthService } from '../../../services/auth';
import { ClearService } from '../../../services/clear';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  isCitiesLoading: boolean = false;

  constructor(public navCtrl: NavController,
              private modalCtrl: ModalController,
              private profileS: ProfileService,
              private photoViewer: PhotoViewer,
              private pogruzkiS: PogruzkiService,
              private authService: AuthService,
              private clearService: ClearService,
              private events: Events,
              public navParams: NavParams) {

       if(!this.profileS.profileSavedData){
          let showLoader = true;
          this.profileS.getProfileData(showLoader); 
       }
       
  }


  ionViewWillEnter(){
    let reload = this.navParams.get('reload');
    if(reload){
        this.profileS.getProfileData(true); 
    }

    let newToken = JSON.parse(localStorage.getItem("gte"));
    if(newToken["token"] !== this.profileS.gte.token){
        let showLoader = true;
        this.profileS.getProfileData(showLoader);
        this.profileS.gte.token = newToken["token"];
    }


    this.isCitiesLoading = true;
    this.profileS.getSubscribedCities().then(()=>{
      this.isCitiesLoading = false;
    }, ()=>{
      this.isCitiesLoading = false;
    })
  }


  doRefresh(refresher) {
    let showLoader = false;
    this.profileS.getProfileData(showLoader).then(res=>{
      refresher.complete();
    })
  }
  

  onEdit(){
    this.navCtrl.push(RegFormPage, {"mode": "Edit"});
  }


  onViewPhoto(photo: string, title: string){
    console.log(photo);
    this.photoViewer.show(photo, title, {share: true});
  }


  unsubscribeCity(type: number, city){
    this.pogruzkiS.unsubscribeCity(type, city).then(res=>{
      if(type == 0){
        let index = this.profileS.inCities.indexOf(city);
        if(index > -1){
          this.profileS.inCities.splice(index, 1);
        }
      }else if(type == 1){
        let index = this.profileS.outCities.indexOf(city);
        if(index > -1){
          this.profileS.outCities.splice(index, 1);
        }
      }
    }, err => {})
  }


  onLogout(){
    localStorage.removeItem("gte");
  
    this.authService .isUserLogedIn = false;
    this.authService.fromPogruzka = false
    this.authService.closeModal = false;
    this.authService.userIdForPush = "";
    // this.menuLists();
    this.clearService.clearVar();
    this.events.publish('user:login', {'logedIn': false});
  }

}
