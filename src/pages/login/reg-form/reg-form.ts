import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Platform, ViewController, ModalController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AuthService } from '../../../services/auth';
import { NaytiPage } from '../../pogruzki/nayti/nayti';
import { ProfileService } from '../../../services/profile';
import { AutoPage } from '../auto/auto';

@IonicPage()
@Component({
  selector: 'page-reg-form',
  templateUrl: 'reg-form.html',
})
export class RegFormPage {

  private regForm: FormGroup;

  public base65Image1: string;
  public base65Image2: string;
  public base65Image3: string;

  mode: string = "New";
  title: string = "Регистрация";
  btnText: string = "Регистрироваться";
  selectedAutoId: number = null;
  selectedAutoName: string = "";
  userForm: {email: string, inn: string, isnds: string, name: string, code_ati: string, type_auto: string, vin: string};
  isReadonly: boolean = true;
  

  constructor(public navCtrl: NavController,
              private formBuilder: FormBuilder,
              private platform: Platform,
              public actionSheetCtrl: ActionSheetController,
              private camera: Camera,
              private viewCtrl: ViewController,
              private profileS: ProfileService,
              private modalCtrl: ModalController,
              private authS: AuthService,
              public navParams: NavParams) {
    
    this.title = "Регистрация";
    this.btnText = "Регистрироваться";
    this.mode = this.navParams.get('mode');
    this.initRegForm();
    
    console.log("LoginPage");
  }

  onRegNewUser(){
    if(this.mode == "New"){
      console.log(this.regForm.value);
      this.authS.isUserLogedIn = true;
      this.navCtrl.setRoot(NaytiPage);
    }else if(this.mode == "Edit"){
      console.log(this.regForm.value);
      this.profileS.profileSavedData.email = this.regForm.value.email;
      this.profileS.profileSavedData.inn = this.regForm.value.inn;
      this.profileS.profileSavedData.nds = this.regForm.value.isnds;
      this.profileS.profileSavedData.name = this.regForm.value.name;
      this.profileS.profileSavedData.code_ati = this.regForm.value.code_ati;
      this.profileS.profileSavedData.type_auto.name = this.selectedAutoName;
      this.profileS.profileSavedData.vin = this.regForm.value.vin;
      

      let creds = {
        "email": this.regForm.value.email,
        "inn": this.regForm.value.inn,
        "nds": this.regForm.value.isnds,
        "name": this.regForm.value.name,
        "code_ati": this.regForm.value.code_ati,
        "type_auto": {
          "id": this.selectedAutoId,
          "name": "" 
        },
        'vin': this.profileS.profileSavedData.vin,
        "photo_passport": this.profileS.profileSavedData.photo_passport,
        "photo_vu": this.profileS.profileSavedData.photo_vu,
        "photo_sts": this.profileS.profileSavedData.photo_sts,
      };
      console.log(creds);
      this.profileS.updateUserData(creds).then((res: any)=>{
        this.viewCtrl.dismiss({reload: true});
      }, (error: any)=>{
        this.profileS.presentToast(error.message, 2000, 'top');
        console.log(error);
      })
    }
  }

  onClose(){
    this.viewCtrl.dismiss();
  }


  onSelectAuto(){
    const autoModal = this.modalCtrl.create(AutoPage, {'mode': "auto"});
    autoModal.onDidDismiss((data: any)=>{
      if(data){
        this.regForm.controls["type_auto"].setValue(data.item.name);
        this.selectedAutoName = data.item.name;
        this.selectedAutoId = data.item.id;
      };
    })
    autoModal.present();
  };


  onClickCompany(){
    const autoModal = this.modalCtrl.create(AutoPage, {"mode": "company"});
    
    autoModal.onDidDismiss((data)=>{
      if(data){
        if(data.item.inn && data.item.inn !== "null"){ //SELECTED company
          this.regForm.controls['name'].setValue(data.item.name);
          this.regForm.controls['inn'].setValue(data.item.inn);
          this.regForm.controls['isnds'].setValue(data.item.nds);
          this.regForm.controls['code_ati'].setValue(data.item.code_ati);
          this.isReadonly = true;
        }else{ // NEW COMPANY
          this.regForm.controls['inn'].setValue(data.item);
          this.regForm.controls['name'].setValue("");
          this.regForm.controls['isnds'].setValue(1);
          this.regForm.controls['code_ati'].setValue(data.item.code_ati);
          this.isReadonly = false;
        }
      }
    });

    autoModal.present();
  }



  onClickPhoto(type: number){
      const actionSheet = this.actionSheetCtrl.create({
        buttons: [
          {
            text: 'Сделать фото',
            icon: !this.platform.is('ios') ? 'camera' : null,
            cssClass: 'selectionsIcon',
            handler: () => {
              this.takeGetPic(true, type);
            }
          },
          {
            text: 'Выбрать фото',
            cssClass: 'selectionsIcon',
            icon: !this.platform.is('ios') ? 'ios-image' : null,
            handler: () => {
              this.takeGetPic(false, type);
            }
          },
          {
            text: 'Отмена',
            role: 'cancel',
            cssClass: 'cancelIcon',
            icon: !this.platform.is('ios') ? 'close' : null,
            handler: () => {
            }
          }
        ]
      });
    actionSheet.present();
  }


  
  takeGetPic(isTake: boolean, type: number){
    
    let opt = this.camera.PictureSourceType.CAMERA;
    if(!isTake){
      opt = this.camera.PictureSourceType.PHOTOLIBRARY;
    }
    
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: opt,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 800,
      targetHeight: 800,
      allowEdit: true,
      correctOrientation:true
    };
  
    this.camera.getPicture(options).then((imageData) => {
      if(type == 1){
        this.profileS.profileData.photo_passport = 'data:image/jpeg;base64,' + imageData;
      }else if(type == 2){
        this.profileS.profileData.photo_vu = 'data:image/jpeg;base64,' + imageData;
      }else if(type == 3){
        this.profileS.profileData.photo_sts = 'data:image/jpeg;base64,' + imageData;
      }
    }, (err) => {});

  }



  initRegForm(){

      this.userForm = {
        email: '',
        inn: null,
        isnds: '',
        name: '',
        code_ati: null,
        type_auto: '',
        vin: ''
      };
      // Edit Mode
      if(this.mode == "Edit"){
        this.profileS.profileData = this.profileS.profileSavedData;
        console.log(this.profileS.profileData);
        this.userForm = {
          email: this.profileS.profileData.email,
          inn: this.profileS.profileData.inn.toString(),
          isnds: this.profileS.profileData.nds.toString(),
          name: this.profileS.profileData.name,
          code_ati: this.profileS.profileData.code_ati,
          type_auto: this.profileS.profileData.type_auto.name,
          vin: this.profileS.profileData.vin
        };
        
        this.selectedAutoId = this.profileS.profileData.type_auto.id;
        this.selectedAutoName = this.profileS.profileData.type_auto.name;
        
        this.title = "Редактирование профиля";
        this.btnText = "Сохранить";
      }
      
      this.regForm = this.formBuilder.group({
        email:     [this.userForm.email, [Validators.required]],
        inn:       [this.userForm.inn, [Validators.required]],
        isnds:     [this.userForm.isnds, [Validators.required]],
        name:      [this.userForm.name, [Validators.required]],
        type_auto: [this.userForm.type_auto, [Validators.required]],
        code_ati:  [this.userForm.code_ati, [Validators.required]],
        vin:       [this.userForm.vin, [Validators.required]]
      });

    }




}