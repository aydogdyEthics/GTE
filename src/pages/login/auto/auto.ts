import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController, Searchbar } from 'ionic-angular';
import { Company } from '../../../models/auth/company';
import { Auto } from '../../../models/auth/autoType';
import { AuthService } from '../../../services/auth';

@IonicPage()
@Component({
  selector: 'page-auto',
  templateUrl: 'auto.html',
})
export class AutoPage {

  @ViewChild('searchbar') searchbar:Searchbar;

  autoArray: Array<Auto>
  companyArray: Array<Company>;
  mode: string = "company";
  title: string = "Поиск компании";
  queryText: string = "";
  isAnyCompany: boolean = true;

  constructor(private authS: AuthService,
              private navParams: NavParams, 
              private viewCtrl: ViewController) {
    
        this.mode = this.navParams.get('mode');            
        
        if(this.mode == "auto") {
          this.title = "Список авто";
          this.authS.getArrayTs().then((res: any)=>{
            console.log(res);
            this.autoArray = res.data.items;
          });
        }
  
  }

  ionViewDidEnter() {
      if(this.mode !== "auto"){
        setTimeout(() => {
          this.searchbar.setFocus();
        },300);
      }
  }

  
  getItems(ev: any){
    this.queryText = "";
    let val = ev.target.value.replace(/\s/g,'');
        if(!val){
          this.companyArray = [];
        }
        if (val && val.trim() != '') {
          this.authS.getArrayCompany(val).then((res: any)=>{
            this.queryText = val;
            console.log(res);
            this.companyArray = res.data.items;
            if(this.companyArray.length > 0){
              this.isAnyCompany = true;
            }else{
              if(this.queryText && this.queryText.trim() != '')
              this.isAnyCompany = false;
            }
          });
        }
  }


  onSelectCompany(company: Company){
    this.viewCtrl.dismiss({'item': company});
  }

  onSaveNew(){
    this.viewCtrl.dismiss({'item': this.queryText});
  }

  onSelectAuto(auto: Auto){
    this.viewCtrl.dismiss({'item': auto});
  }

  onClose(){
    this.viewCtrl.dismiss();
  }

}
