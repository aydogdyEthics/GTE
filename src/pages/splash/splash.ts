import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {

  constructor(public navCtrl: NavController,
              public splashScreen: SplashScreen,
              public viewCtrl: ViewController,
              public navParams: NavParams) {
  }

  ionViewDidEnter() {
      // this.splashScreen.hide();
      //  setTimeout(() => {
      //    this.viewCtrl.dismiss();
      //  }, 2000);
    
     }
}
