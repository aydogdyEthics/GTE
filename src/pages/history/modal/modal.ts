import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { State } from '../../../models/pogruzki/state';


@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  statesArray: Array<State>;
  
  constructor(public navParams: NavParams) {

      let items = this.navParams.get("items");
      this.statesArray = items;
  }

}
