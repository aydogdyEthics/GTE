import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { PogruzkaPage } from '../pogruzka/pogruzka';
import { PogruzkiService } from '../../../services/pogruzki';
import { AuthService } from '../../../services/auth';
import { PogruzkaList } from '../../../models/pogruzki/pogruzkaList';

@IonicPage()
@Component({
  selector: 'page-pogruzki',
  templateUrl: 'pogruzki.html',
})
export class PogruzkiPage{

  pogruzkiArray: Array<PogruzkaList> = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private pogruzkiS: PogruzkiService,
              private loadingCtrl: LoadingController,
              private authS: AuthService) {

      if(this.pogruzkiS.isObratnyePogruzkiPage && this.pogruzkiS.showBackOrders){
        this.pogruzkiS.getMyBackPoints(true);
        console.log("getMyBackPoints--------------");
      }

      this.pogruzkiS.gte = JSON.parse(localStorage.getItem("gte"));
      if(this.pogruzkiS.gte && this.pogruzkiS.gte !== null){
        this.authS.isUserLogedIn = true;
      }else{
        this.authS.isUserLogedIn = false;
      }

      this.pogruzkiArray = this.pogruzkiS.pogruzkiArray;
  }

  ionViewWillEnter(){
    this.pogruzkiArray = this.pogruzkiS.pogruzkiArray;
    this.authS.fromPogruzka = false;
  }
  
  doRefresh(refresher) {
    console.log("getMyBackPoints--------------");
    if(this.pogruzkiS.showBackOrders){
      this.pogruzkiS.getMyBackPoints(false).then((res: any)=>{
        console.log(res);
        this.pogruzkiArray = res.data.items;
        this.pogruzkiS.pogruzkiArray = res.data.items;
        refresher.complete();
      }, error=>{
        refresher.complete();
      })
    }
  }

  onSelectPogruzka(id: number, offer_price: number, indx: number){
    const loading = this.loadingCtrl.create();
    loading.present();

    this.pogruzkiS.onGetOrderInfo(id).then((res:any)=>{
      this.navCtrl.push(PogruzkaPage, {"offer_price": offer_price, "indx": indx});
      loading.dismiss();
    },(error:any)=>{
      loading.dismiss();
    })
  }

}