import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController, ToastController } from 'ionic-angular';
import { PogruzkiService } from '../../../services/pogruzki';
import { Pogruzka } from '../../../models/pogruzki/pogruzka';
import { AuthService } from '../../../services/auth';
import { LoginPage } from '../../login/login/login';
import { ProfileService } from '../../../services/profile';
import { Keyboard } from '@ionic-native/keyboard';

import { Validators, FormGroup, FormBuilder } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-pogruzka',
  templateUrl: 'pogruzka.html',
})
export class PogruzkaPage {
  
  suggestPrice: string;
  precent: number;
  summary: number;
  topliva: number;
  selectedIndx: number;
  isPrice: boolean = true;
  isBackBtnClicked = false;
  p: Pogruzka;

  private priceForm: FormGroup;

  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              public navParams: NavParams,
              private authS: AuthService,
              private formBuilder: FormBuilder,
              private modalCtrl: ModalController,
              private profileS: ProfileService,
              private keyboard: Keyboard,
              public toastCtrl: ToastController, 
              private pogruzkiS: PogruzkiService) {

        this.priceForm = this.formBuilder.group({
          suggestPrice: ['']
        });

        this.p = this.pogruzkiS.selectedPogruzka;
        this.precent = parseInt(this.p.offer_percent);
        this.summary = this.navParams.get("offer");
        this.topliva = 0;
        this.isPrice = true;
        if(this.navParams.get("offer_price") > 0){
          this.suggestPrice = this.p.offer_price;
          this.priceForm.controls["suggestPrice"].setValue(this.suggestPrice);
        }
        this.selectedIndx = this.navParams.get("indx");
        this.calcPrice(1);

        

        this.keyboard.hideKeyboardAccessoryBar(true);
  }

  ionViewWillEnter(){
    this.pogruzkiS.gte = JSON.parse(localStorage.getItem("gte"));
    if(this.pogruzkiS.gte && this.pogruzkiS.gte !== 'null'){
      this.profileS.gte = this.pogruzkiS.gte;
    }
    console.log("ionViewWillEnter");
  }


  onBack(){
    this.navCtrl.pop();
    console.log("Back");
  }
  

  onSend(){
     // if user not logged in
    if(!this.authS.isUserLogedIn){
          const alert = this.alertCtrl.create({
            title: 'Внимание!',
            message: 'Чтобы предложить цену, войдите в приложение.',
            buttons: [
              {
                text: 'Отмена',
                role: 'cancel',
                handler: () => {}
              },
              {
                text: 'Войти',
                handler: () => {
                    if(!this.authS.isUserLogedIn){
                      const loginModal = this.modalCtrl.create(LoginPage);
                      loginModal.present();
                      this.authS.fromPogruzka = true;
                    }else{
                      this.authS.fromPogruzka = true;
                      this.navCtrl.setRoot(LoginPage);
                    }
                }
              }
            ]
          });
          alert.present();

          // this.priceForm.controls["suggestPrice"].setValue(this.suggestPrice);
    }else{

        const loading = this.loadingCtrl.create();
        loading.present();  
          let creds;
          this.pogruzkiS.gte = JSON.parse(localStorage.getItem("gte"));
          if(this.pogruzkiS.gte.nds && this.pogruzkiS.gte.nds !== 'null' && this.pogruzkiS.gte.nds == 2){
            creds = {
              "summary":  this.priceForm.controls["suggestPrice"].value,
              "percent": this.precent
            };
          }else{
            creds = {
              "summary":  this.priceForm.controls["suggestPrice"].value
            };
          }

          this.pogruzkiS.sendUserPrice(creds).then((res: any)=>{
              console.log(res);
              this.pogruzkiS.pogruzkiArray[this.selectedIndx].offer_price = res.data.offer_price;
              this.pogruzkiS.pogruzkiArray[this.selectedIndx].offer_company = parseInt(res.data.offer_company);
              this.presentToast(res.data.message, "success", 'bottom', false, "", 3000);
              loading.dismiss();
          },(error: any)=>{
              this.presentToast(error.message, "error", 'bottom', false, "", 3000);
              loading.dismiss();
          })

    }

  }

  presentToast(message: string, classType: string, position:string, showCloseButton:boolean, closeButtonText: string, time: number) {
      let toast = this.toastCtrl.create({
        message: message,
        position: position,
        showCloseButton: showCloseButton,
        closeButtonText: closeButtonText,
        cssClass: classType,
        duration: time,
        dismissOnPageChange: true
      });

      toast.present();
  }

  calcPrice(withTopliva: number){
    this.summary = 0;
    if(this.precent == 0){
      this.topliva = 0; 
    }
    if( this.priceForm.controls["suggestPrice"].value == null || ! this.priceForm.controls["suggestPrice"].value){
      this.topliva = 0;
      this.isPrice = true;
    }
    if(parseInt(this.priceForm.controls["suggestPrice"].value) > 0){
        // Цена без топлива, компания без ндс:
        this.isPrice = false;
        if(this.precent == 0){ //false
            this.summary = parseInt( this.priceForm.controls["suggestPrice"].value) + parseInt( this.priceForm.controls["suggestPrice"].value)*18/100;
        }else{
          //Цена с топливом, компания без ндс:
          if(this.precent>0){
            this.topliva = (parseInt( this.priceForm.controls["suggestPrice"].value)/100)*this.precent;
            let nnn = parseInt( this.priceForm.controls["suggestPrice"].value)-parseInt( this.priceForm.controls["suggestPrice"].value)*this.precent/100;
            let nds = nnn*18/100;
            this.summary = nnn + nds + this.topliva;
          }else{
            this.topliva = 0;
            this.summary = parseInt( this.priceForm.controls["suggestPrice"].value) + (parseInt( this.priceForm.controls["suggestPrice"].value)/100)*18;
          }
        }
    }
  }




}
