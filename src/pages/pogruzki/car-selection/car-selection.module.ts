import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarSelectionPage } from './car-selection';

@NgModule({
  declarations: [
    CarSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(CarSelectionPage),
  ],
})
export class CarSelectionPageModule {}
