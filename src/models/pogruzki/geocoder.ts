export interface NativeGeocoderResultModel {
    subAdministrativeArea: string,
    postalCode: string,
    locality: string,
    subLocality: string,
    subThoroughfare: string,
    countryCode: string,
    countryName: string,
    administrativeArea: string,
    thoroughfare: string
  }