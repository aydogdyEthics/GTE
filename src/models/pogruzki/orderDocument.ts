export class OrderDocument {

    type: number;
    docs: Array<string>;

    constructor(type: number, docs: Array<string>){
        this.type = type;
        this.docs = docs;
    }

}