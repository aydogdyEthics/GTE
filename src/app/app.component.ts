import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController, AlertController, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Events } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';

import { ProfilePage } from '../pages/login/profile/profile';
import { HistoryPage } from '../pages/history/history/history';
import { AuthService } from '../services/auth';
import { HelpPage } from '../pages/help/help';
import { NaytiPage } from '../pages/pogruzki/nayti/nayti';
import { ClearService } from '../services/clear';
import { MainTabs } from '../pages/moiPogruzki/mainTab';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Network } from '@ionic-native/network';
import { LoginPage } from '../pages/login/login/login';
import { ConfigService } from '../services/config';
import { RootTabPage } from '../pages/root-tab/root-tab';
import { HistoryService } from '../services/history';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
    rootPage: any;
    activePage: any;
    isUserLogedIn: boolean=false;
    isLogedOut: boolean = false;
    oneSignalOrderId: number;

    pages: Array<{title: string, component: any, iosIcon: string, andrIcon: string,}>;
  
    constructor(public platform: Platform,
                public statusBar: StatusBar,
                public splashScreen: SplashScreen,
                private authS: AuthService,
                private clearS: ClearService,
                public modalCtrl: ModalController,
                private network: Network,
                private alertCtrl: AlertController,
                private diagnostic: Diagnostic,
                private historyS: HistoryService,
                private configS: ConfigService,
                private oneSignal: OneSignal,
                private app: App,
                private events: Events) {

      this.initialApp();
                  
      this.events.subscribe('user:login', () => {
        this.menuLists();
      });

    }
  
    ngOnInit(){
      this.menuLists();
    }

    menuLists(){
      let userToken = JSON.parse(localStorage.getItem("gte"));
        if(!userToken && userToken == null){
            console.log("FALSE");
            this.events.publish('user:loged:in', {'isLogedIn': false});
        }else{
          console.log("TRUE");
          this.authS.isUserLogedIn = true;
          this.events.publish('user:loged:in', {'isLogedIn': true});
        }
        this.rootPage = RootTabPage;
    }


    initialApp() {
      this.platform.ready().then(() => {
        this.statusBar.styleDefault();        
          this.checkGPS();
          // this.oneSignalConf();
          this.checkInternetConnection();    
      });
    }


    oneSignalConf(){

          this.oneSignal.startInit('e9613918-23f4-471b-a357-524ea0152bbd', '316836843621'); //OneSignalAppId (Keys & IDs), Firebase-Идентификатор отправителя(Cloud messaging)
                                  
          this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
  
          this.oneSignal.handleNotificationReceived().subscribe((data: any) => {
            console.log("handleNotificationReceived", data);
            this.oneSignalOrderId = data.payload.additionalData.orderId;
          });
          
          this.oneSignal.handleNotificationOpened().subscribe((data) => { // when Push notification tepped, will go to MainPage 
            console.log("handleNotificationOpened", data);
            let nav = this.app.getRootNav(); 
            nav.setRoot(RootTabPage, {'index': 1, 'fromPush': true, 'orderId': this.oneSignalOrderId})
          });
  
          this.oneSignal.endInit();
  
          this.oneSignal.getIds().then((ids) =>{ // user id for sending PUSH
              console.log("OneSignal ids:", ids);
              this.authS.userIdForPush = ids.userId;
          });
    }


    openPage(page) {
      this.nav.setRoot(page.component);
      this.activePage = page;
    }

    checkActive(page){
      return page == this.activePage;
    }

    checkInternetConnection(){
      this.network.onDisconnect().subscribe(() => {
        console.log('network was disconnected :-(');
        this.showNetworkAlert();
      });
  
      this.network.onConnect().subscribe(res=>{
        console.log('network was connected :-)');
      });
    }


    showNetworkAlert() {    
      let networkAlert = this.alertCtrl.create({
        title: 'Нет соединение с интернетом!',
        message: 'Пожалуйста проверьте соединение к интернету.',
        buttons: [
          {
            text: 'Отмена',
            handler: () => {
            }
          },
          {
            text: 'Настройки',
            handler: () => {
              networkAlert.dismiss().then(() => {
                  this.showSettings();
              })
            }
          }
        ]
      });
      networkAlert.present();
    }


    private showSettings() {
      this.diagnostic.switchToWifiSettings();
    };

    checkGPS(){
      this.diagnostic.isLocationEnabled().then((res)=>{
        if(!res){
          this.gpsAelrt();
        }
      }, (error) =>{
      })
    }

    gpsAelrt() {
      let networkAlert = this.alertCtrl.create({
        title: 'Внимание!',
        message: 'GPS-модуль отключен. Пожалуйста, включите GPS',
        buttons: [
          {
            text: 'OK',
            handler: () => {
            }
          }
        ]
      });
      networkAlert.present();
    }
    
    onLogout(){
        localStorage.removeItem("gte");
      
        this.authS.isUserLogedIn = false;
        this.authS.fromPogruzka = false
        this.isLogedOut = true;
        this.authS.closeModal = false;
        this.authS.userIdForPush = "";
        this.menuLists();
        this.clearS.clearVar();
    }

}
