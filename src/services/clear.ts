import { Injectable } from "@angular/core";
import { PogruzkiService } from "./pogruzki";
import { ProfileService } from "./profile";
import { HistoryService } from "./history";


@Injectable()
export class ClearService {

    constructor(private pogruzkiS: PogruzkiService,
                private profileS: ProfileService,
                private historyS: HistoryService){
    }
    
    clearVar(){
        this.pogruzkiS.pogruzkiArray = [];
        this.pogruzkiS.myPogruzkiArray = [];
        this.pogruzkiS.gte = {};
        this.pogruzkiS.selectedPogruzkaIndx = null;
        this.pogruzkiS.isObratnyePogruzkiPage = false;
        this.pogruzkiS.showBackOrders = false;
        this.pogruzkiS.coordinatesInterval = null;
        this.pogruzkiS.isDriverNearPoint = false;


        this.historyS.historyArray = [];
        
        this.profileS.gte = {};
        this.profileS.profileSavedData = null;
        this.profileS.profileData = null;



    }

   
}