import { Injectable } from "@angular/core";
import { Headers, RequestOptions } from "@angular/http";

@Injectable()
export class ConfigService {

    apiRoot: string;
    headers: Headers;
    options: RequestOptions;
    
    constructor(){
        this.apiRoot = "http://driver.api.logistic.exdan.ru";
        this.headers = new Headers({'Content-Type': 'application/json', 'Accept':'application/json'});
        this.options = new RequestOptions({ headers: this.headers });
    }
    
}