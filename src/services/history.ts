import { Injectable, ViewChild } from "@angular/core";
import { Http } from "@angular/http";
import { LoadingController, Nav } from 'ionic-angular';

import 'rxjs/Rx';
import { Histori } from "../models/pogruzki/history";
import { ConfigService } from "./config";
import { MainTabs } from "../pages/moiPogruzki/mainTab";
import { AuthService } from "./auth";
import { Platform } from 'ionic-angular';

@Injectable()
export class HistoryService {

    historyArray: Array<Histori>=[];
    @ViewChild(Nav) nav: Nav;

	constructor(private http: Http,
                private loadingCtrl: LoadingController,
                private authS: AuthService,
                private configS: ConfigService){
    }

    
    getMyHistory(showLoader: boolean){
        let gte = JSON.parse(localStorage.getItem("gte"));
        const loading = this.loadingCtrl.create();
        if(showLoader) loading.present();

        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/order/history?access_token=` + gte.token;
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    if(showLoader) loading.dismiss();
                    this.historyArray = res.json().data.items;
                    console.log(this.historyArray);
                    resolve(res.json());
                },(msg) => {
                    if(showLoader) loading.dismiss();
                    reject(msg.json());
                });
        });
        return promise;
    }


    getHistoryStates(id: number){
        const loading = this.loadingCtrl.create();
        loading.present();        
        let gte = JSON.parse(localStorage.getItem("gte"));
        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configS.apiRoot}/order/`+id+'/state?access_token=' + gte.token;
            this.http.get(apiURL, this.configS.options)
            .toPromise()
            .then((res) => {
                    loading.dismiss();        
                    resolve(res.json());
                },(msg) => {
                    loading.dismiss();        
                    reject(msg.json());
                });
        });
        return promise;
    }




    

}